import connexion
import json
from flask import request, Response, render_template

PETS = {}

APP = connexion.FlaskApp(__name__, specification_dir="./")
API = None


def get_pets(animal_type, limit=100):
    return [v for v in PETS.values() if v["animal_type"] == animal_type][
        :int(limit)
    ], 200


def get_pet(pet_id):
    content_type = request.headers.get("Accept", "application/json")
    if "text/html" in content_type:
        schema = API.specification["definitions"]["Pet"].copy()
        if pet_id in PETS:
            for k, v in PETS[pet_id].items():
                if k in schema["properties"]:
                    schema["properties"][k]["default"] = v
        data = render_template("pet.html", schema=json.dumps(schema), url=request.url)
        return Response(data, mimetype="text/html")

    try:
        return PETS[pet_id]

    except KeyError:
        return "Pet doesn't exist", 404


def put_pet(pet_id):
    if pet_id in PETS:
        status = 200
    else:
        status = 201
    import pdb; pdb.set_trace()
    PETS[pet_id] = request.json
    PETS[pet_id]["id"] = pet_id
    return PETS[pet_id], status


def delete_pet(pet_id):
    if pet_id in PETS:
        return "Pet deleted", 204

    return "Pet doesn't exist", 404


API = APP.add_api("api.yaml")
APP.run(port=8080)
