swagger: '2.0'
info:
  title: Pet Shop Example API
  version: "0.1"
consumes:
  - application/json
produces:
  - application/json
paths:
  /pets:
    get:
      operationId: app.get_pets
      summary: Get all pets
      parameters:
        - name: animal_type
          in: query
          type: string
          pattern: "^[a-zA-Z0-9]*$"
        - name: limit
          in: query
          type: integer
          minimum: 0
          default: 100
      responses:
        200:
          description: Return pets
          schema:
            type: array
            items:
              $ref: '#/definitions/Pet'
  /pets/{pet_id}:
    get:
      operationId: app.get_pet
      summary: Get a single pet
      parameters:
        - $ref: '#/parameters/pet_id'
      responses:
        200:
          description: Return pet
          schema:
            $ref: '#/definitions/Pet'
        404:
          description: Pet does not exist
    put:
      operationId: app.put_pet
      summary: Create or update a pet
      parameters:
        - $ref: '#/parameters/pet_id'
        - name: pet
          in: body
          schema:
            $ref: '#/definitions/Pet'
      responses:
        200:
          description: Pet updated
        201:
          description: New pet created
    delete:
      operationId: app.delete_pet
      summary: Remove a pet
      parameters:
        - $ref: '#/parameters/pet_id'
      responses:
        204:
          description: Pet was deleted
        404:
          description: Pet does not exist


parameters:
  pet_id:
    name: pet_id
    description: Pet's Unique identifier
    in: path
    type: string
    required: true
    pattern: "^[a-zA-Z0-9-]+$"

definitions:
  Pet:
    type: object
    required:
      - name
      - animal_type
    properties:
      id:
        type: string
        title: id
        description: Unique identifier
        example: "123"
        readOnly: true
      name:
        type: string
        title: Name
        description: Pet's name
        example: "Susie"
        minLength: 1
        maxLength: 100
      animal_type:
        type: string
        title: Animal Type
        description: Kind of animal
        example: "cat"
        minLength: 1
