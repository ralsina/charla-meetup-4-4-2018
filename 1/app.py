import connexion

APP = connexion.FlaskApp(__name__, specification_dir="./")
APP.add_api("api.yaml")
APP.run(port=8080)
