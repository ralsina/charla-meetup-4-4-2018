import connexion
from flask import request

PETS = {}


def get_pets(_type=None, limit=100):
    return [v for v in PETS.items() if v["type"] == _type][:limit]


def get_pet(pet_id):
    try:
        return PETS[pet_id]

    except KeyError:
        return "Pet doesn't exist", 404


def put_pet(pet_id):
    if pet_id in PETS:
        status = 200
    else:
        status = 201
    PETS[pet_id] = request.json
    PETS[pet_id]["id"] = pet_id
    return PETS[pet_id], status


def delete_pet(pet_id):
    if pet_id in PETS:
        return "Pet deleted", 204

    return "Pet doesn't exist", 404


APP = connexion.FlaskApp(__name__, specification_dir="./")
APP.add_api("api.yaml")
APP.run(port=8080)
